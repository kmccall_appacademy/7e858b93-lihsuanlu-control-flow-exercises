# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  charz = str.chars
  charz.each do |char|
    if char.upcase != char
      str.delete!(char)
    end
  end
  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  str_len = str.length
  if str_len%2 == 0
    return str[(str_len/2-1)..str_len/2]
  else
    return str[str_len/2]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  ans = 0
  str.chars.each do |letter|
    if VOWELS.include?(letter)
      ans += 1
    end
  end
  ans
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  if num == 0
    return 1
  else
    ans = 1
    while num > 1
      ans *= num
      num -= 1
    end
    return ans
  end
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  ans_str = ""
  arr.each_index do |index|
    if index != arr.length-1
      ans_str << arr[index] << separator
    else
      ans_str << arr[index]
    end
  end
  ans_str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str = str.chars
  str.each_index do |index|
    if index%2 == 1 #if char is even
      str[index] = str[index].upcase
    else
      str[index] = str[index].downcase
    end
  end
  str.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str = str.split
  str.map! do |word|
    if word.length >= 5
      word = word.reverse
    else
      word = word
    end
  end
  str.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  ans_arr = []
  (1..n).each do |int|
    if int%3 == 0 && int%5 == 0
      ans_arr << "fizzbuzz"
    elsif int%3 == 0
      ans_arr << "fizz"
    elsif int%5 == 0
      ans_arr << "buzz"
    else
      ans_arr << int
    end
  end
  ans_arr

end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.sort.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  if num == 1
    return false
  else
    #if argument not divisible by any number less than half of it, it is prime
    (2..num/2).each do |int|
      if num%int == 0
        return false
      end
    end
    return true
  end
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  arr_of_factors = []
  (1..num).each do |int|
    if num%int == 0
      arr_of_factors << int
    end
  end
  arr_of_factors
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  arr_of_factors = factors(num)
  ans = []
  arr_of_factors.each do |int|
    if prime?(int)
      ans << int
    end
  end
  ans
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  #determine if oddball is even or odd
  flag = -1
  oddcount = 0
  evencount = 0
  index = 0
  while flag == -1
    if arr[index]%2 == 0
      evencount += 1
      if evencount > 1
        flag =0
      end
    else
      oddcount += 1
      if oddcount > 1
        flag = 1
      end
    end
    index += 1
  end
  arr.each do |int|
    if int%2 != flag
      return int
    end
  end
end
